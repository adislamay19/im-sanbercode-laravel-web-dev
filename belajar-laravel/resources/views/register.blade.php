@extends('layouts.master')

@section('title')
 Halaman Register   
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="POST">
    @csrf
    <label>First name:</label><br>
    <input type="text" name="fname"><br><br>
    
    <label>Last name:</label><br>
    <input type="text" name="lname"><br><br>
    
    <label>Gender</label><br>
    <input type="radio" value="1" name="gender"> Male <br>
    <input type="radio" value="2" name="gender"> Female <br>
    <input type="radio" value="3" name="gender"> Other <br><br>

    <label>Nationality :</label><br><br>
        <select name="nationality">
        <option value="1">Indonesian</option>
        <option value="2">Singapure</option>
        <option value="3">Malaysian</option>
        <option value="4">Australian</option>
        </select><br><br>

    <label>Language Spoken</label><br>
        <input type="checkbox" value="1" name="language"> Bahasa Indonesia <br>
        <input type="checkbox" value="2" name="language"> English <br>
        <input type="checkbox" value="3" name="language"> Other <br><br>
        
    <label>Bio :</label><br>
    <textarea name="bio" rows="10" cols="20"></textarea><br><br>
    
    <input type="submit" value="Sign Up">
  </form>
@endsection
