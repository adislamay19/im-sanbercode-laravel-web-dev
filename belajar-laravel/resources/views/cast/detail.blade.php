@extends('layouts.master')

@section('title')
    Halaman Create Cast
@endsection

@section('content')
    <h1>{{ $casts->name }}</h1>
    <p> Umur : {{ $casts->umur }}</p>
    <p> Bio : {{ $casts->bio }}</p>
@endsection