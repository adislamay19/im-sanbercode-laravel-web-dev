@extends('layouts.master')

@section('title')
    Halaman Edit Cast
@endsection

@section('content')
<form action="/cast/{{ $casts->id }}" method="POST">
    @csrf
    @method('put')
      <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $casts->name }}">
        
        @error('name')
        <div class= 'invalid-feedback'>
          {{ $message }}
        </div>
        @enderror
      
      </div>
      
      <div class="form-group">
        <label>Umur</label>
        <input type="text" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{ $casts->umur }}">
      
        @error('umur')
            <div class= 'invalid-feedback'>
              {{ $message }}
            </div>
            @enderror
  
      </div>
      
      <div class="form-group">
        <label>Bio</label>
        <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" rows="3">{{ $casts->bio }}</textarea>
       
        @error('bio')
        <div class= 'invalid-feedback'>
          {{ $message }}
        </div>
        @enderror
      
      </div>
      
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection