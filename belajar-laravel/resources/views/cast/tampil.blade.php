@extends('layouts.master')

@section('title')
    Halaman Tampil Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-sm btn-primary">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="colspan">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($casts as $key => $value)
        <tr>
            <th scope="row">{{ $key + 1 }}</th>
            <td>{{ $value->name }}</td>
            <td>    
                <form action="/cast/{{ $value->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <a class="btn btn-info btn-sm" href="/cast/{{ $value->id }}">Detail</a>
                    <a class="btn btn-warning btn-sm" href="/cast/{{ $value->id }}/edit">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
        @empty
            <tr>
                <th>Tidak Ada Cast</th>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection