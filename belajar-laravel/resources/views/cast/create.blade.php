@extends('layouts.master')

@section('title')
    Halaman Create Cast
@endsection

@section('content')
<form action="/cast" method="POST">
  @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Name">
      
      @error('name')
      <div class= 'invalid-feedback'>
        {{ $message }}
      </div>
      @enderror
    
    </div>
    
    <div class="form-group">
      <label>Umur</label>
      <input type="text" class="form-control @error('umur') is-invalid @enderror" name="umur" placeholder="Umur">
    
      @error('umur')
          <div class= 'invalid-feedback'>
            {{ $message }}
          </div>
          @enderror

    </div>
    
    <div class="form-group">
      <label>Bio</label>
      <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" rows="3" placeholder="Tuliskan Bio Anda Disini"></textarea>
     
      @error('bio')
      <div class= 'invalid-feedback'>
        {{ $message }}
      </div>
      @enderror
    
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection