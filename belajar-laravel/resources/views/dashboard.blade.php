@extends('layouts.master')

@section('title')
    Halaman Dashboard
@endsection

@section('content')
    <h1>Selamat Datang {{$namaDepan}} {{ $namaBelakang }}</h1>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
@endsection
