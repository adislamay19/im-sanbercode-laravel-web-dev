<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{   
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $validated = $request->validate([
            'name' => 'required|alpha|min:5',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('casts')->insert([
            'name' => $request->input('name'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        return redirect('/cast');
    }

    public function index(){
        $casts = DB::table('casts')->get();

        return view('cast.tampil', ['casts' => $casts]);
    }

    public function show($id){
        $casts = DB::table('casts')->find($id);

        return view('cast.detail', ['casts' => $casts]);
    }

    public function edit($id){
        $casts = DB::table('casts')->find($id);

        return view('cast.edit', ['casts' => $casts]);
    }

    public function update($id, Request $request){

        $request->validate([
            'name' => 'required|alpha|min:5',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('casts')
              ->where('id', $id)
              ->update([
                'name' => $request->input('name'),
                'umur' => $request->input('umur'),
                'bio' => $request->input('bio')
              ]);
        
              return redirect('/cast');
    }

    public function destroy($id){
        DB::table('casts')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
